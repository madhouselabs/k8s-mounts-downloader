import os
import subprocess
import json
import sys
import shlex
from pyfzf.pyfzf import FzfPrompt

def sysCall(cmd, **kwargs):
    c = shlex.split(cmd)
    out = subprocess.check_output(['/usr/bin/zsh', '-c', cmd], shell=False, **kwargs)
    return out.decode("utf-8")

fzf = FzfPrompt()

def useFzf(options):
    try:
        return fzf.prompt(options)
    except KeyboardInterrupt:
        sys.exit(1);

ds = json.loads(sysCall("kubectl get deploy -o json"))

deployments = []
m = dict()
for item in ds['items']:
    m[item['metadata']['name']] = item['spec']['template']['spec']['containers']
    deployments.append(item['metadata']['name'])

deploymentName = useFzf(deployments)[0]

cmap = {}
cnames = []
for item in m[deploymentName]:
    cnames.append(item['name'])
    cmap[item['name']] = item

containerName = useFzf(cnames)[0]

container = cmap[containerName]

envs = [env for env in container['env']]
volumeMounts = [vm for vm in container['volumeMounts']]

for vm in volumeMounts:
    mp = vm['mountPath']
    dest = f"/tmp/kloudlite"
    os.makedirs(dest, exist_ok=True)
    sysCall(f"kubectl exec deploy/{deploymentName} -c {containerName} -- tar cf - {mp} | tar xf - -C {dest}", stderr = False)

print("DONE, your volume mounts are at /tmp/kloudlite")
